package com.company.example;

public class Car extends Vehicle{
    public int carVariable;
    public void carMethod(){
        System.out.println("i'm a car");
    }

    @Override
    public String toString() {
        return "Car{" +
                "carVariable=" + carVariable +
                ", vehicleVariable=" + vehicleVariable +
                '}';
    }
}
